; Copyright 1996 Acorn Computers Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
; > GetAll

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:CPU.Arch
        GET     Hdr:Machine.<Machine>
        GET     Hdr:HALSize.<HALSize>
        GET     Hdr:ImageSize.<ImageSize>
        GET     Hdr:UserIF.<UserIF>
        $GetCPU
        $GetIO
        $GetMEMC
        $GetMEMM
        $GetVIDC

        GET     hdr.Options

; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; now get the headers
; ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        GET     Hdr:CMOS
        GET     Hdr:Heap
        GET     Hdr:Sprite
        GET     Hdr:PublicWS
        GET     Hdr:HALEntries
        GET     Hdr:HALDevice
        GET     Hdr:OSEntries
        GET     Hdr:Services
        GET     Hdr:FSNumbers
        GET     Hdr:HighFSI
        GET     Hdr:NewErrors
        GET     Hdr:Proc
        GET     Hdr:RS423
        GET     Hdr:ModHand
        GET     Hdr:Variables
        GET     Hdr:EnvNumbers
        GET     Hdr:UpCall
        GET     Hdr:Sound
        GET     Hdr:Pointer
        GET     Hdr:Podule
        GET     Hdr:VduExt
        GET     Hdr:Buffer
        GET     Hdr:Font
        GET     Hdr:DevNos
        GET     Hdr:OsBytes
        GET     Hdr:Internatio
        GET     Hdr:Territory
        GET     Hdr:Countries
        GET     Hdr:Portable
        GET     Hdr:MsgTrans
        GET     Hdr:PaletteV
        GET     Hdr:GraphicsV
        GET     Hdr:VIDCList
        GET     Hdr:Wimp
        GET     Hdr:ColourTran
        GET     Hdr:Debug
        GET     Hdr:FileTypes
        GET     Hdr:RTC
        GET     Hdr:SerialOp
        GET     Hdr:Keyboard
        GET     Hdr:OSMem
        GET     Hdr:OSMisc
        GET     Hdr:OSRSI6
        GET     Hdr:PL310

; now the main parts of the MOS

        GET     hdr.KernelWS
        GET     hdr.KeyWS
        GET     hdr.Copro15ops ; some macros
        GET     hdr.ARMops

        GET     s.Kernel
        GET     s.ARMops
        GET     s.NewIRQs
        GET     s.Oscli
        GET     s.SysComms
        GET     s.HeapMan
        GET     s.ModHand
        $GetUnsqueeze
        GET     s.ArthurSWIs
        $GetKernelMEMC
        GET     s.Exceptions
        GET     s.ChangeDyn
        GET     s.HAL
        GET     s.Arthur2
        GET     s.LibKern
        GET     s.Utility
        GET     s.MoreComms
        GET     s.Convrsions
        GET     s.MoreSWIs
        GET     s.ExtraSWIs
        GET     s.HeapSort
        GET     s.Arthur3
        GET     s.SWINaming
        GET     s.TickEvents
        GET     s.NewReset
        $GetMessages
        GET     s.Middle
        GET     s.Super1
        GET     s.MemInfo
        GET     s.CPUFeatures
        GET     s.MemMap2
        ! 0, "Main kernel size = &" :CC: :STR: (.-KernelBase)
StartOfVduDriver
        GET     vdu.s.VduDecl
        GET     vdu.s.legacymodes
        GET     vdu.s.VduDriver
        GET     vdu.s.VduSWIs
        GET     vdu.s.VduPalette
        GET     vdu.s.vdupalxx
        GET     vdu.s.VduPlot
        GET     vdu.s.VduGrafA
        GET     vdu.s.VduGrafB
        GET     vdu.s.VduGrafC
        GET     vdu.s.VduGrafD
        GET     vdu.s.VduGrafE
        GET     vdu.s.VduGrafF
        GET     vdu.s.VduGrafG
        GET     vdu.s.VduGrafH
        GET     vdu.s.VduGrafI
        GET     vdu.s.VduGrafJ
        GET     vdu.s.VduGrafK
        GET     vdu.s.VduGrafL
        GET     vdu.s.VduGrafV
        GET     vdu.s.VduGrafHAL
        GET     vdu.s.VduWrch
        GET     vdu.s.Vdu23
        GET     vdu.s.VduPointer
        GET     vdu.s.Vdu5
        GET     vdu.s.VduCurSoft
        GET     vdu.s.VduTTX
        GET     vdu.s.VduFontL1

        ! 0, "Vdu section size = &" :CC: :STR: (.-StartOfVduDriver)

StartOfPMF
        GET     PMF.s.osinit
        GET     PMF.s.oseven
        GET     PMF.s.osbyte
        GET     PMF.s.osword
        GET     PMF.s.realtime
        GET     PMF.s.i2cutils
        GET     PMF.s.IIC
        GET     PMF.s.oswrch
        GET     PMF.s.buffer
        GET     PMF.s.key
        GET     PMF.s.mouse
        ALIGN
EndOfPMF

        ! 0, "PMF section size = &" :CC: :STR: (EndOfPMF - StartOfPMF)

StartOfAMB
        GET     AMBControl.s.AMB
EndOfAMB

        ! 0, "AMB section size = &" :CC: :STR: (EndOfAMB - StartOfAMB)

EndOfKernel
        DCD     0

        END
