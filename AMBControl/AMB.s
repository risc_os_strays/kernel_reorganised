; Copyright 1996 Acorn Computers Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

; Implements OS_AMBControl (takes care of application memory management for Wimp)
;
; Nov 1995  mjs  development version originated as Module
; Jul 1996  mjs  fully implemented in kernel for RISC OS 3.70
; Mar 1997  mjs  performance enhancements for Ursula OS
; Oct 2000  mjs  Ursula code resurrected for 32-bit HALised kernel, hoorah!

        GET     AMBControl.s.main
        GET     AMBControl.s.allocate
        GET     AMBControl.s.deallocate
        GET     AMBControl.s.growshrink
        GET     AMBControl.s.handler
        GET     AMBControl.s.mapslot
        GET     AMBControl.s.mapsome
        GET     AMBControl.s.readinfo

        GET     AMBControl.s.growp
        GET     AMBControl.s.shrinkp

        GET     AMBControl.s.Memory

        GET     AMBControl.s.memmap

        END
